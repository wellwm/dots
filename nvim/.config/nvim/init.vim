" Let's start with adding numbering!
:set number

" Or even relative numbering
" Let's leave it a comment, but you can uncomment of course
:set relativenumber

" Just adding a variable which defines how many spaces tab will be equal to
:set tabstop=4

" Also add a softtabstop to make it relative - if you're on 2 spaces - it will
" tab to 4 anyway!
:set softtabstop=1
" It works!
:set encoding=UTF-8

" Let's also add a package manager!
" Its strings are here:
call plug#begin()
" We just add plugin's repo after 'Plug'
" Plugins and other stuff can be found on vimawesome.com (that's not an
" advertise!)
" No tomorrow theme? No problems, let's add our own!

"Plug 'flazz/vim-colorschemes'

" Now looks gorgeous!
" Let's add a few more plug-ins!
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdtree'
Plug 'ryanoasis/vim-devicons'
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
"Plug 'mhinz/vim-startify'
Plug 'kassio/neoterm'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'rafi/awesome-vim-colorschemes'
Plug 'dracula/vim', { 'as': 'dracula' }
Plug 'https://github.com/folke/tokyonight.nvim'
Plug 'htpps://github.com/morhetz'

call plug#end()

" Autostart
autocmd VimEnter * :colorscheme gruvbox
autocmd VimEnter * :AirlineTheme gruvbox

" Keymaps
nnoremap <C-q> :close<CR>
nnoremap <C-S> :w<CR>
nnoremap <C-Q> :q<CR>
"nnoremap <C-s> :Startify<CR>
nnoremap <C-n> :NERDTreeToggle<CR>
