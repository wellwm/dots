const char *colorname[] = {

  /* 8 normal colors */
  [0] = "#040715", /* black   */
  [1] = "#1B4454", /* red     */
  [2] = "#2C4A56", /* green   */
  [3] = "#3A5A62", /* yellow  */
  [4] = "#4D5055", /* blue    */
  [5] = "#666665", /* magenta */
  [6] = "#8B443F", /* cyan    */
  [7] = "#bcb2ad", /* white   */

  /* 8 bright colors */
  [8]  = "#837c79",  /* black   */
  [9]  = "#1B4454",  /* red     */
  [10] = "#2C4A56", /* green   */
  [11] = "#3A5A62", /* yellow  */
  [12] = "#4D5055", /* blue    */
  [13] = "#666665", /* magenta */
  [14] = "#8B443F", /* cyan    */
  [15] = "#bcb2ad", /* white   */

  /* special colors */
  [256] = "#040715", /* background */
  [257] = "#bcb2ad", /* foreground */
  [258] = "#bcb2ad",     /* cursor */
};

/* Default colors (colorname index)
 * foreground, background, cursor */
 unsigned int defaultbg = 0;
 unsigned int defaultfg = 257;
 unsigned int defaultcs = 258;
 unsigned int defaultrcs= 258;
