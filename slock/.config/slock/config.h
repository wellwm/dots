/* user and group to drop privileges to */
static const char *user  = "x";
static const char *group = "wheel";

//#include "/home/x/.config/slock/colors/purplish.h"
//#include "/home/x/.config/slock/colors/suckless.h"
#include "/home/x/.config/slock/colors/lantern.h"
//#include "/home/x/.config/slock/colors/gruvbox.h"
//#include "/home/x/.config/slock/colors/monochromatic.h"

/* treat a cleared input like a wrong password (color) */
static const int failonclear = 0;

/*
// default message
static const char * message = "Screen is locked - try to unlock!";

// text color
static const char * text_color = "#eeeeee";

// text size (must be a valid size)
static const char * text_size = "12x20";
*/
