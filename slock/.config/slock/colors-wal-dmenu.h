static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bcb2ad", "#040715" },
	[SchemeSel] = { "#bcb2ad", "#1B4454" },
	[SchemeOut] = { "#bcb2ad", "#8B443F" },
};
