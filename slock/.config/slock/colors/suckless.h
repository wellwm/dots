static const char *colorname[NUMCOLS] = {
	[INIT] =   "#222222",   //   after initialization
//	[INPUT] =  "#C35EFF",   //   during input
	[INPUT] =  "#005577",   //   during input
	[FAILED] = "red",       //   wrong password
};
