static const char *colorname[NUMCOLS] = {
	[INIT] =   "#111111",   //   after initialization
	[INPUT] =  "#b9b9b9",   //   during input
	[FAILED] = "#252525",       //   wrong password
};
