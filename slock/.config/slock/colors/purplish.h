static const char *colorname[NUMCOLS] = {
	[INIT] =   "#080B21",   //   after initialization
	[INPUT] =  "#D8E2EF",   //   during input
	[FAILED] = "#CD23B9",   //   wrong password
};
