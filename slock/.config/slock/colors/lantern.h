static const char *colorname[NUMCOLS] = {
	[INIT] =   "#261b17",   //   after initialization
	[INPUT] =  "#e4cbb3",   //   during input
	[FAILED] = "#e01d1d",       //   wrong password
};
