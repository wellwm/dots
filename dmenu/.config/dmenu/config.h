/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts[] = {
	"SauceCodePro NF:size=11"
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */

//#include "/home/x/.config/dmenu/colors/monochromatic.config.h"
//#include "/home/x/.config/dmenu/colors/colors-pywal.h"
//#include "/home/x/.config/dmenu/colors/purplish.h"
//#include "/home/x/.config/dmenu/colors/suckless.config.h"
//#include "/home/x/.config/dmenu/colors/gruvbox.config.h"
#include "/home/x/.config/dmenu/colors/lantern.config.h"

/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/* Size of the window border */
static const unsigned int border_width = 5;
