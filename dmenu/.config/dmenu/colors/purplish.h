static const char *colors[SchemeLast][2] = {
	/*               fg         bg       */
	[SchemeNorm] = { "#D7E2EF", "#080B21" },
	[SchemeSel]  = { "#FFFFFF", "#CD23B9" },
	[SchemeOut]  = { "#000000", "#CD23B9" },
};
