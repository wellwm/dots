static const char *colors[SchemeLast][2] = {
	/*               fg         bg       */
	[SchemeNorm] = { "#999999", "#101010" },
	[SchemeSel]  = { "#b9b9b9", "#252525" },
	[SchemeOut]  = { "#101010", "#454545" },
};
