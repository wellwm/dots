static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#b39f8b", "#261b17" },
	[SchemeSel]  = { "#e4cbb3", "#f99666" },
	[SchemeOut]  = { "#261b17", "#f99666" },
};
