static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#bdae93", "#282828" },
	[SchemeSel]  = { "#fbf1c7", "#fe8019" },
	[SchemeOut]  = { "#1d2021", "#fe8019" },
};
