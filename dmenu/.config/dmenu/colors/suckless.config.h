static const char *colors[SchemeLast][2] = {
	/*               fg         bg       */
	[SchemeNorm] = { "#bbbbbb", "#222222" },
//	[SchemeSel]  = { "#eeeeee", "#005577" },
	[SchemeSel]  = { "#eeeeee", "#7987d2" },
	[SchemeOut]  = { "#000000", "#7987d2" },
};
