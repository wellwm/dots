/* Terminal colors (16 first used in escape sequence) */
static const char *colorname[] = {
	/* 8 normal colors */
	"#101010",
	"#cf9595",
	"#69A964",
	"#BCAC5E",
	"#595778",
	"#977697",
	"#7DBDB6",
	"#999999",

	/* 8 bright colors */
	"#454545",
	"#CF9595",
	"#99CF95",
	"#DBC86F",
	"#837EC9",
	"#C49CC3",
	"#98C8CD",
	"#b9b9b9",

	[255] = 0,

	/* more colors can be added after 255 to use with DefaultXX */
	"#b9b9b9",
	"#252525",
	"#999999", /* default foreground colour */
	"#101010", /* default background colour */
};

/*
 * Default colors (colorname index)
 * foreground, background, cursor, reverse cursor
 */
unsigned int defaultfg = 258;
unsigned int defaultbg = 259;
unsigned int defaultcs = 256;
static unsigned int defaultrcs = 257;

