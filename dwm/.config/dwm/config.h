/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
//static const unsigned int gappx     = 5;        /* gaps between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int user_bh            = 30;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = { "SauceCodePro NF:size=11" };
static const char dmenufont[]       = "SauceCodePro NF:size=11";
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */

// Colors
//#include "/home/x/.config/dwm/colors/dracula.config.h"
//#include "/home/x/.config/dwm/colors/gruvbox.config.h"
#include "/home/x/.config/dwm/colors/lantern.config.h"
//#include "/home/x/.config/dwm/colors/suckless.config.h"
//#include "/home/x/.config/dwm/colors/purplish.config.h"
//#include "/home/x/.config/dwm/colors/monochrome.config.h"
//#include "/home/x/.config/dwm/colors/monochromatic.config.h"
//#include "/home/x/.config/dwm/colors/myown.config.h"
//#include "/home/x/.config/dwm/colors/nord.config.h"
//#include "/home/x/.config/dwm/colors/onedark.config.h"
//#include "/home/x/.config/dwm/colors/pywal1.config.h"
//#include "/home/x/.config/dwm/colors/pywal2.config.h"

static const char *const autostart[] = {
	"sh", "/home/x/.config/dwm/au.sh", NULL,
	NULL /* terminate */
};

/* tagging */
//static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
//static const char *tags[] = { "+:", "(o)", "C", ">_", "r/u", "#", "LL", ">", "..." };
//static const char *tags[] = { " gaming", " www", " dev", " term/edit", "ﳨ rise", "# msg", " edu", " video", "etc" };
static const char *tags[] = { "", "", "", "", "", "", "", "", "", "..." };
//static const char *tags[] = { "1:  gaming", "2:  www", "3:  dev", "4:  term/edit", "5: ﳨ rise", "6: # msg", "7:  edu", "8:  video", "9: etc" };
//static const char *tags[] = { "一", "二", "三", "四", "五", "六", "七", "八", "九" };
//static const char *tags[] = { " 一 ", " 二 ", " 三 ", " 四 ", " 五 ", " 六 ", " 七 ", " 八 ", " 九 " };
//static const char *tags[] = { "一 gam", "二 www", "三 dev", "四 trm", "五 ris", "六 msg", "七 mus", "八 vid", "九 etc" };
//static const char *tags[] = { " 一 gam ", " 二 www ", " 三 dev ", " 四 trm ", " 五 ris ", " 六 msg ", " 七 mus ", " 八 vid ", " 九 etc " };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                             instance    title       tags mask     isfloating   monitor */
	{ "Gimp",                            NULL,       NULL,       1 << 9,       1,           -1 },
	{ "Firefox",                         NULL,       NULL,       1 << 1,       0,           -1 },
	{ "Brave",                           NULL,       NULL,       1 << 1,       0,           -1 },
	{ "librewolf",                       NULL,       NULL,       1 << 1,       0,           -1 },
	{ "libreoffice-startcenter",         NULL,       NULL,       1 << 8,       0,           -1 },
	{ "libreoffice",                     NULL,       NULL,       1 << 8,       0,           -1 },
	{ "Libreoffice",                     NULL,       NULL,       1 << 8,       0,           -1 },
	{ "FreeTube",                        NULL,       NULL,       1 << 7,       0,           -1 },
	{ "Nitrogen",                        NULL,       NULL,       1 << 4,       0,           -1 },
	{ "Lxappearance",                    NULL,       NULL,       1 << 4,       0,           -1 },
	{ "lxappearance",                    NULL,       NULL,       1 << 4,       0,           -1 },
	{ "discord",                         NULL,       NULL,       1 << 5,       0,           -1 },
	{ "Steam",                           NULL,       NULL,       1 << 0,       0,           -1 },
	//{ NULL,                              NULL,       NULL,       0,            0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG)												\
	&((Keychord){1, {{MODKEY, KEY}},								view,           {.ui = 1 << TAG} }), \
		&((Keychord){1, {{MODKEY|ControlMask, KEY}},					toggleview,     {.ui = 1 << TAG} }), \
		&((Keychord){1, {{MODKEY|ShiftMask, KEY}},						tag,            {.ui = 1 << TAG} }), \
		&((Keychord){1, {{MODKEY|ControlMask|ShiftMask, KEY}},			toggletag,      {.ui = 1 << TAG} }),

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
//static const char *termcmd[]  = { "st", NULL };
//static const char *termcmd[] = { "tabbed", "-r", "2", "st", "-w", "''", NULL };
static const char *termcmd[] = { "alacritty", NULL };
static char *alacritty[]     = { "alacritty", NULL };
static char *st[]            = { "st", NULL };
static char *tabst[]         = { "tabbed", "-r", "2", "st", "-w", "''", NULL };
//static char *roficmd[]       = { "rofi", "-show", "drun", NULL };
static char *emacs[]         = { "emacsclient", "-c", "-a", "'emacs'", NULL };
static char *vim[]           = { "st", "-e", "nvim", NULL };
static char *vide[]          = { "neovide", NULL };
static char *office[]        = { "libreoffice", NULL };
//static char *keepassxc[]     = { "keepassxc", NULL };
static char *screenshot1[]   = { "screenshot", NULL };
static char *screenshot2[]   = { "screenshot-fullscreen", NULL };
//static char *screenshot[] = { "flameshot", "gui", NULL };
static char *qutebrowser[]   = { "qutebrowser", NULL };
static char *librewolf[]     = { "librewolf", NULL };
static char *firefox[]       = { "firefox", NULL };
static char *freetube[]      = { "freetube", NULL };
static char *slock[]         = { "slock", NULL };
static char *pass[]          = { "passmenu", NULL };
static char *pipey[]         = { "st", "-e", "pipe-viewer", NULL };
static char *pulsemixer[]    = { "st", "-e", "pulsemixer", NULL };
static char *nitrogen[]      = { "nitrogen", NULL };
static char *lxapp[]         = { "lxappearance", NULL };

// Games
static char *steam[]         = { "steam", NULL };
static char *stk[]           = { "supertuxkart", NULL };
static char *oa[]            = { "openarena", NULL };
static char *xonotic[]       = { "xonotic-sdl", NULL };
static char *orc[]           = { "openra-cnc", NULL };
static char *orr[]           = { "openra-ra", NULL };
static char *ord[]           = { "openra-d2k", NULL };

static Keychord *keychords[] = {
	/* Keys        function        argument */
	&((Keychord){2, {{MODKEY|ShiftMask, XK_r}, {MODKEY, XK_n}}, spawn,			{.v = nitrogen } }),
	&((Keychord){2, {{MODKEY|ShiftMask, XK_r}, {MODKEY, XK_l}}, spawn,			{.v = lxapp } }),
   	&((Keychord){2, {{MODKEY, XK_s}, {MODKEY, XK_y}},		    spawn,          {.v = pipey } }),
   	&((Keychord){1, {{MODKEY|ShiftMask, XK_e}},		            spawn,          {.v = emacs } }),
   	&((Keychord){2, {{MODKEY, XK_e}, {MODKEY, XK_e}},		    spawn,          {.v = emacs } }),
   	&((Keychord){2, {{MODKEY, XK_e}, {MODKEY, XK_v}},		    spawn,          {.v = vim } }),
   	&((Keychord){2, {{MODKEY, XK_e}, {MODKEY, XK_n}},		    spawn,          {.v = vim } }),
   	&((Keychord){2, {{MODKEY, XK_e}, {MODKEY|ShiftMask, XK_v}},	spawn,          {.v = vide } }),
   	&((Keychord){2, {{MODKEY, XK_e}, {MODKEY|ShiftMask, XK_n}}, spawn,          {.v = vide } }),
	&((Keychord){1, {{MODKEY|ShiftMask, XK_p}},		            spawn,          {.v = screenshot1 } }),
	&((Keychord){1, {{MODKEY|ControlMask, XK_p}},		        spawn,          {.v = screenshot2 } }),
	&((Keychord){1, {{MODKEY, XK_q}},                      		spawn,          {.v = librewolf } }),
	&((Keychord){2, {{MODKEY|ShiftMask, XK_b}, {MODKEY, XK_q}}, spawn,          {.v = qutebrowser } }),
	&((Keychord){2, {{MODKEY|ShiftMask, XK_b}, {MODKEY, XK_f}}, spawn,          {.v = firefox } }),
	&((Keychord){2, {{MODKEY|ShiftMask, XK_b}, {MODKEY, XK_l}}, spawn,          {.v = librewolf } }),
	&((Keychord){2, {{MODKEY|ShiftMask, XK_b}, {MODKEY|ShiftMask, XK_f}}, spawn, {.v = freetube } }),
	&((Keychord){2, {{MODKEY|ShiftMask, XK_m}, {MODKEY, XK_m}},	spawn,          {.v = pulsemixer } }),
	&((Keychord){2, {{MODKEY|ShiftMask, XK_s}, {MODKEY, XK_p}}, spawn,          {.v = pass } }),
	&((Keychord){2, {{MODKEY|ShiftMask, XK_s}, {MODKEY, XK_s}}, spawn,          {.v = slock } }),
	&((Keychord){1, {{MODKEY, XK_p}},							spawn,          {.v = dmenucmd } }),

	&((Keychord){2, {{MODKEY, XK_g}, {MODKEY, XK_s}},           spawn,          {.v = steam } }),
	&((Keychord){2, {{MODKEY, XK_g}, {MODKEY|ShiftMask, XK_s}}, spawn,          {.v = stk } }),
	&((Keychord){2, {{MODKEY, XK_g}, {MODKEY|ShiftMask, XK_o}}, spawn,          {.v = oa } }),
	&((Keychord){2, {{MODKEY, XK_g}, {MODKEY, XK_x}},           spawn,          {.v = xonotic } }),
	&((Keychord){2, {{MODKEY, XK_g}, {MODKEY, XK_o}, {XK_c}},   spawn,          {.v = orc } }),
	&((Keychord){2, {{MODKEY, XK_g}, {MODKEY, XK_o}, {XK_r}},   spawn,          {.v = orr } }),
	&((Keychord){2, {{MODKEY, XK_g}, {MODKEY, XK_o}, {XK_d}},   spawn,          {.v = ord } }),

	&((Keychord){1, {{MODKEY|ShiftMask, XK_o}},					spawn,          {.v = office } }),

	&((Keychord){1, {{MODKEY|ShiftMask, XK_Return}},			spawn,          {.v = termcmd } }),
	&((Keychord){2, {{MODKEY|ShiftMask, XK_t}, {MODKEY, XK_a}},	spawn,          {.v = alacritty } }),
	&((Keychord){2, {{MODKEY|ShiftMask, XK_t}, {MODKEY, XK_s}},	spawn,          {.v = st } }),
	&((Keychord){2, {{MODKEY|ShiftMask, XK_t}, {MODKEY|ShiftMask, XK_t}},	spawn,          {.v = tabst } }),

	&((Keychord){1, {{MODKEY, XK_b}},							togglebar,      {0} }),
	&((Keychord){1, {{MODKEY, XK_j}},							focusstack,     {.i = +1 } }),
	&((Keychord){1, {{MODKEY, XK_k}},							focusstack,     {.i = -1 } }),
	&((Keychord){1, {{MODKEY, XK_i}},							incnmaster,     {.i = +1 } }),
	&((Keychord){1, {{MODKEY, XK_d}},							incnmaster,     {.i = -1 } }),
	&((Keychord){1, {{MODKEY, XK_h}},							setmfact,       {.f = -0.05} }),
	&((Keychord){1, {{MODKEY, XK_l}},							setmfact,       {.f = +0.05} }),
	&((Keychord){1, {{MODKEY, XK_Return}},						zoom,           {0} }),
	&((Keychord){1, {{MODKEY, XK_Tab}},							view,           {0} }),
	&((Keychord){1, {{MODKEY|ShiftMask, XK_c}},					killclient,     {0} }),
	&((Keychord){1, {{MODKEY, XK_t}},							setlayout,      {.v = &layouts[0]} }),
	&((Keychord){1, {{MODKEY, XK_f}},							setlayout,      {.v = &layouts[1]} }),
	&((Keychord){1, {{MODKEY, XK_m}},							setlayout,      {.v = &layouts[2]} }),
	&((Keychord){1, {{MODKEY, XK_space}},						setlayout,      {0} }),
	&((Keychord){1, {{MODKEY|ShiftMask, XK_space}},				togglefloating, {0} }),
//	&((Keychord){1, {{MODKEY, XK_0}},							view,           {.ui = ~0 } }),
//	&((Keychord){2, {{MODKEY, XK_t}, {XK_1}},					view,           {.ui = 10 } }),
//	&((Keychord){1, {{MODKEY|ShiftMask, XK_0}},					tag,            {.ui = ~0 } }),
	&((Keychord){1, {{MODKEY, XK_comma}},						focusmon,       {.i = -1 } }),
	&((Keychord){1, {{MODKEY, XK_period}},						focusmon,       {.i = +1 } }),
	&((Keychord){1, {{MODKEY|ShiftMask, XK_comma}},				tagmon,         {.i = -1 } }),
	&((Keychord){1, {{MODKEY|ShiftMask, XK_period}},			tagmon,         {.i = +1 } }),
/*	&((Keychord){1, {{MODKEY, XK_minus}},						setgaps,       {.i = -1 } }),
	&((Keychord){1, {{MODKEY, XK_equal}},						setgaps,       {.i = +1 } }),
	&((Keychord){1, {{MODKEY|ShiftMask, XK_equal}},				setgaps,       {.i = 0 } }),
	&((Keychord){1, {{MODKEY|ControlMask, XK_equal}},			setgaps,       {.i = 5 } }), */
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	TAGKEYS(                        XK_0,                      9)
	&((Keychord){1, {{MODKEY|ShiftMask, XK_q}},					quit,           {0} }),
	&((Keychord){1, {{MODKEY|ShiftMask, XK_q}},					quit,           {1} }),
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
//	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

