static const char col_gray1[]       = "#101010";
static const char col_gray2[]       = "#454545";
static const char col_gray3[]       = "#999999";
static const char col_gray4[]       = "#b9b9b9";
static const char col_gray5[]       = "#252525";
static const char *colors[][3]      = {
  /*               fg         bg         border   */
  [SchemeNorm] = { col_gray3, col_gray1, col_gray1 },
  [SchemeSel]  = { col_gray4, col_gray5,  col_gray2  },
};

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_gray5, "-sf", col_gray4, "-l", "20", NULL };
