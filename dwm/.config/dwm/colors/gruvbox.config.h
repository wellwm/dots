static const char col_gray[]        = "#504945";
static const char col_gray2[]       = "#282828";
static const char col_gray3[]       = "#fbf1c7";
static const char col_gray4[]       = "#bdae93";
static const char col_cyan[]        = "#fe8019";

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray4, col_gray2, col_gray  },
	[SchemeSel]  = { col_gray3, col_cyan,  col_cyan  },
};

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray2, "-nf", col_gray4, "-sb", col_cyan, "-sf", col_gray3, "-l", "20", NULL };
