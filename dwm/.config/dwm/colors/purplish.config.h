static const char col_gray1[]       = "#080B21";
static const char col_gray2[]       = "#363A59";
static const char col_gray3[]       = "#FFFFFF";
static const char col_cyan[]        = "#CD23B9";

static const char *colors[][3]      = {
//	                 fg         bg         border
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray3, col_cyan,  col_cyan  },
};

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray3, "-l", "20", NULL };
