static const char col_gray1[]       = "#282c34";
static const char col_gray2[]       = "#282c34";
static const char col_gray3[]       = "#d7d7d7";
static const char col_cyan[]        = "#924441";

static const char *colors[][3]      = {
//	                 fg         bg         border
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray3, col_cyan,  col_cyan  },
};

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray1, /*"-l", "20",*/ NULL };
