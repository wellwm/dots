static const char col_gray[]       = "#f8f8f2";
static const char col_gray2[]       = "#282a36";

// Purple
static const char col_cyan[]        = "#bd93f9";

// Orange
//static const char col_cyan[]        = "#ffb86c";

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_cyan, col_gray2, col_gray  },
	[SchemeSel]  = { col_gray2, col_cyan, col_cyan  },
};

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray2, "-nf", col_cyan, "-sb", col_cyan, "-sf", col_gray2, /*"-l", "20",*/ NULL };
