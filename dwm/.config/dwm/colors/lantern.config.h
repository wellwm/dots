static const char col_gray[]        = "#532f28";
static const char col_gray2[]       = "#261b17";
static const char col_gray3[]       = "#e4cbb3";
static const char col_gray4[]       = "#b39f8b";
static const char col_cyan[]        = "#f99666";

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray4, col_gray2, col_gray  },
	[SchemeSel]  = { col_gray3, col_cyan,  col_cyan  },
};

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray2, "-nf", col_gray4, "-sb", col_cyan, "-sf", col_gray3, "-l", "20", NULL };
