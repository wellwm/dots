static const char norm_fg[] = "#bcb2ad";
static const char norm_bg[] = "#040715";
static const char norm_border[] = "#837c79";

static const char sel_fg[] = "#bcb2ad";
static const char sel_bg[] = "#2C4A56";
static const char sel_border[] = "#bcb2ad";

static const char urg_fg[] = "#bcb2ad";
static const char urg_bg[] = "#1B4454";
static const char urg_border[] = "#1B4454";

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", norm_bg, "-nf", norm_fg, "-sb", sel_bg, "-sf", sel_fg, /*"-l", "20",*/ NULL };

static const char *colors[][3]      = {
    /*               fg           bg         border                         */
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
//    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};
