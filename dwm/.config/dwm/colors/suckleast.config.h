static const char col_gray1[]       = "#000000";
static const char col_gray2[]       = "#242424";
static const char col_gray3[]       = "#ffffff";

static const char col_cyan[]        = "#0000ff";
//static const char col_cyan[]        = "#ff00ff";
//static const char col_cyan[]        = "#ffa500";

static const char *colors[][3]      = {
//	                 fg         bg         border
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray3, col_cyan,  col_cyan  },
};

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray3, /*"-l", "20",*/ NULL };
