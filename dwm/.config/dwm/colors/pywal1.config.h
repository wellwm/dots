static const char norm_fg[] = "#e6dccf";
static const char norm_bg[] = "#1f150f";
static const char norm_border[] = "#a19a90";

static const char sel_fg[] = "#1f150f";
//static const char sel_fg[] = "#e6dccf";
static const char sel_bg[] = "#CFA376";
static const char sel_border[] = "#CFA376";


static const char *colors[][3]      = {
//                   fg           bg         border
    [SchemeNorm] = { norm_fg,     norm_bg,   norm_border }, // unfocused wins
    [SchemeSel]  = { sel_fg,      sel_bg,    sel_border },  // the focused win
//    [SchemeUrg] =  { urg_fg,      urg_bg,    urg_border },
};

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", norm_bg, "-nf", norm_fg, "-sb", sel_bg, "-sf", sel_fg, /*"-l", "20",*/ NULL };
