static const char col_gray1[]       = "#242427";
static const char col_gray2[]       = "#888894";
static const char col_gray3[]       = "#B3B3Bf";

static const char *colors[][3]      = {
//	                 fg         bg         border
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray1, col_gray3, col_gray3 },
};

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_gray3, "-sf", col_gray1, /*"-l", "20",*/ NULL };
