static const char col_gray[]       = "#939293";
static const char col_gray2[]       = "#2d2a2e";
static const char col_cyan[]        = "#fcfcfa";

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_cyan, col_gray2, col_gray  },
	[SchemeSel]  = { col_gray2, col_cyan, col_cyan  },
};

static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray2, "-nf", col_cyan, "-sb", col_cyan, "-sf", col_gray2, /*"-l", "20",*/ NULL };
