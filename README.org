#+title: Hello. It's just my dots repo.
#+description: Here you can find some suckless utilities configured by me.
#+author: wellwm

*   News.
 + I deleted old setups because now I have a better way to handle multiple colorschemes at the same utility - .org files and .h "#include"
 + Added a new theme
*   Features
 + Color palettes builtin to suckless programmes configs.
 + Using several patches for dwm
*   Q/A
**  Why my repo?
Because it contains everything to start - dwm, nvim, alacritty and dmenu. What else do you need?
**  How to install it in a few commands?
+ Install _GNU Stow_ through your distro's package manager.
+ #+begin_src
  stow
  #+end_src
+ #+begin_src
  sudo make clean install -C ~/.config/dwm/
  sudo make clean install -C ~/.config/dmenu/
  sudo make clean install -C ~/.config/st/
  sudo make clean install -C ~/.config/tabbed/
  sudo make clean install -C ~/.config/slock/
  #+end_src
+ AND PUT THIS FOLDER IN SAFE PLACE WHERE IT CANNOT BE REMOVED, OTHERWISE YOUR SETUP (except suckless stuff) WILL GET BONKADONKED
